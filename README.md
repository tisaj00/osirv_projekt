# PROJEKT

## Pretvorba slike iz RGB sustava u druge sustave boja

### Opis zadatka : Teorteski opisati RGB, HSV, CMY I YcbC sustav boja. Napisati python kod za implementaciju algoritma koji
ce biti u mogucnosti pretvoriti RGB sliku u HSV, CMY te YcbC sustave boja.
Integrirati napisane plugineove u SIMPL. Prikazati dobivene rezultate na proizvoljno odabranim slikama.